/** @jsx React.DOM */

// Create a custom component by calling React.createClass.

var Countdown = React.createClass({

    getInitialState: function(){
        return { seconds: 0, pomodori: 0 };
    },

    componentDidMount: function(){
		this.setState({seconds: this.props.seconds});
        this.timer = setInterval(this.tick, 1000);
    },

    componentWillUnmount: function(){
        clearInterval(this.timer);
    },

    tick: function(){
        this.setState({seconds: this.state.seconds - 1});
		if(this.state.seconds < 0) {
			this.setState({seconds: this.props.seconds});
			this.setState({pomodori: this.state.pomodori + 1});
		}
    },

    render: function() {
		return (
				<div>
					<p>Countdown time: {this.state.seconds}</p> 
					<p>Pomodori Completed: {this.state.pomodori}</p>
				</div>
		);
    }
});

React.render(
    <Countdown seconds="25" pomodori="0" />,
    document.body
);